from django.db import models
from django.core.validators import RegexValidator

alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
# Create your models here.
class User(models.Model):
    id = models.CharField(max_length=20, blank=False, null=False, validators=[alphanumeric], primary_key=True)
    real_name = models.CharField(max_length=50, blank=False, null=False)
    tz = models.CharField(max_length=50, blank=False, null=False)
    # activity_periods = models.ForeignKey(ActivityPeriod,on_delete=models.CASCADE,default=None)
    def __str__(self):
        return (self.id + self.real_name + self.tz)
    
class ActivityPeriod(models.Model):
    start_time = models.DateTimeField(blank=False, null=False,unique=True)
    end_time = models.DateTimeField(blank=False, null=False)
    user = models.ForeignKey(User,related_name='activity_periods',on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'start_time']
    def __str__(self):
        return ('start_time '+str(self.start_time) + ' - end_time ' + str(self.end_time))
