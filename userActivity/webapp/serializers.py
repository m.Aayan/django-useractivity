from rest_framework import serializers
from . models import User
from . models import ActivityPeriod

    
class ActivityPeriodSerializer(serializers.ModelSerializer):
    class Meta:
        model=ActivityPeriod
        fields = '__all__'
        
class UserSerializer(serializers.ModelSerializer):
    activity_periods=ActivityPeriodSerializer(many=True)
    class Meta:
        model = User
        fields =['id','real_name','tz','activity_periods']
        # id = serializers.CharField(max_length=20)
        # real_name = serializers.CharField(max_length=50)
        # tz = serializers.CharField(max_length=50)