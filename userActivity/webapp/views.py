from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . models import User
from . serializers import UserSerializer

# Create your views here.
def home(request):
    return HttpResponse("<h1>Welcome to User Activity Api</h1>")

class UserList(APIView):
    def get(self, request):
        userList = User.objects.all()
        print(userList)
        serializer = UserSerializer(userList, many=True)
        print(serializer.data)
        return Response(serializer.data)
    
    def post(self):
        pass